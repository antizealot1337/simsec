package main

import (
	"flag"
	"fmt"
	"os"

	"bitbucket.org/antizealot1337/simsec"
)

func main() {
	var remove bool

	// Setup the command line arguments
	flag.BoolVar(&remove, "delete", false, "Delete the files after zeroing")

	// Parse the command line args
	flag.Parse()

	// Set the usage
	flag.Usage = func() {
		// Print the usage
		fmt.Printf("USAGE: %s [OPTIONS] FILES...\n", os.Args[0])

		// Show the defaults
		flag.PrintDefaults()
	} //func

	// Make sure files were passed to the program
	if flag.NArg() == 0 {
		// Show the usage
		flag.Usage()

		// Exit
		return
	} //if

	// Loop through the command line arguments
	for _, fname := range flag.Args() {
		// Bleach the file
		if err := simsec.Bleach(fname); err != nil {
			// Write the error
			fmt.Fprintf(os.Stderr, "Error zeroing %s: %v\n", fname, err)

			continue
		} //if

		// Check if the file should be removed
		if remove {
			if err := os.Remove(fname); err != nil {
				// Write the error
				fmt.Fprintf(os.Stderr, "Error deleting %s: %v\n", fname, err)
			} //if
		} //if
	} //for
} //main
