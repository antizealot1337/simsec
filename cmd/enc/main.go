package main

import (
	"flag"
	"fmt"
	"os"

	"bitbucket.org/antizealot1337/interact"
	"bitbucket.org/antizealot1337/simsec"
)

func main() {
	// The key
	var key string
	var keep, bleach bool

	// Setup the command line arguments
	showVer := flag.Bool("version", false, "Print the version")
	dec := flag.Bool("decrypt", false, "Decrypt instead of encrypt")
	flag.StringVar(&key, "key", "", "The key")
	flag.BoolVar(&keep, "keep", false, "Keep the original file")
	flag.BoolVar(&bleach, "quick", true,
		"Don't bleach the original file before deleting")

	// Setup the usage command
	flag.Usage = func() {
		fmt.Printf("USAGE: %s [OPTIONS] FILES...\n", os.Args[0])
		flag.PrintDefaults()
	} //func

	// Check if the project was called as dec. If so it is assumed it should
	// decrypt the files instead of encrypt.
	if os.Args[0] == "dec" {
		*dec = true
	} //if

	// Parse the command line
	flag.Parse()

	// Check if showing the version
	if *showVer {
		// TODO: Show the version and return
		return
	} //if

	// Request the key
	if key == "" {
		key = getKey()
	} //if

	// Make sure the key isn't empty
	if key == "" {
		fmt.Fprintln(os.Stderr, "Key cannot be empty")
		return
	} //if

	// Loop through the remaining args and treate them as files
	for _, file := range flag.Args() {
		// Check if decrypting or encrypting
		if *dec {
			simsec.DecryptFile(file, key)
		} else {
			simsec.EncryptFile(file, key)
		} //if

		// Check if the file should be bleached or zeroed
		if bleach {
			// Bleach the file
			simsec.Bleach(file)
		} //if

		// Check if the file should be delete
		if !keep {
			os.Remove(file)
		} //if
	} //for
} //func

func getKey() string {
	// Get the password
	pass, err := interact.Password("Please enter a key: ")

	// Check for an error
	if err != nil {
		panic(err)
	} //if

	// Return the line
	return pass
} //getKey
