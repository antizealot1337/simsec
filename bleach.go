package simsec

import (
	"errors"
	"io"
	"os"
)

const (
	defaultBuffSize = 1024
)

// Bleach a file will write random data to a file.
func Bleach(file string) error {
	// Open the file
	f, err := os.OpenFile(file, os.O_WRONLY, 0644)

	// Check for an error
	if err != nil {
		return errors.New("Open error due to " + err.Error())
	} //if

	// Make sure the file is closed at exit
	defer f.Close()

	// Set the file pointer to the start of the file
	_, err = f.Seek(0, os.SEEK_SET)

	// Check for an error
	if err != nil {
		return errors.New("Seek error due to " + err.Error())
	} //if

	// Stat the file
	stat, err := f.Stat()

	// Check for an error
	if err != nil {
		return errors.New("Stat error due to" + err.Error())
	} //if

	// Write zeros to the file the file
	return writeZeros(f, stat.Size(), defaultBuffSize)
} //func

func writeZeros(out io.Writer, size, buffSize int64) error {
	// Check if the file size is less than the buffer size
	if size <= buffSize {
		// Create the buffer
		buff := make([]byte, size)

		// Write the buffer to the file
		_, err := out.Write(buff)

		// Check the error
		if err != nil {
			return zeroErrCtx(err)
		} //if

		// Return any error
		return nil
	} //if

	// Create a buffer
	buff := make([]byte, buffSize)

	// Calculate how many times to write the buffer to the file
	times := size / buffSize

	// Write the buffer for the loop
	for i := int64(0); i < times; i++ {
		// Write the buffer
		_, err := out.Write(buff)

		// Check for an error
		if err != nil {
			return zeroErrCtx(err)
		} //if
	} //if

	// Write the remainder
	_, err := out.Write(buff[:size%buffSize])

	// Check the error
	if err != nil {
		return zeroErrCtx(err)
	} //if

	// Return any error
	return nil
} //func

func zeroErrCtx(original error) error {
	return errors.New("Zeroing error due to " + original.Error())
} //func
