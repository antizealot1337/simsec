package simsec

import (
	"bytes"
	"crypto/aes"
	"testing"
)

func TestEncyptAndDecrypt(t *testing.T) {
	// The cipher key
	key := "example test key 1234567"

	// Create two byte buffers
	var plain, sec bytes.Buffer

	// Create a cipher block
	block, err := aes.NewCipher([]byte(key))

	// Check for an error
	if err != nil {
		t.Fatal(err)
	} //if

	// Write a plain text message to the plain buffer
	plain.WriteString("test")

	// Encrypt the data
	if err := Encrypt(&plain, &sec, block); err != nil {
		t.Fatal(err)
	} //if

	// Clear the plain buffer
	plain.Reset()

	//Decrypt the data
	if err := Decrypt(&sec, &plain, block); err != nil {
		t.Fatal(err)
	} //if

	// Make sure the text was decrypted
	if expected, actual := "test", plain.String(); actual != expected {
		t.Errorf(`Expected (d)ecryption to yield "%s" but was "%s"`, expected,
			actual)
	} //if
} //func

func TestHasSSExt(t *testing.T) {
	// Check if a bad file name is missing the extension
	if fn := "no_good"; hasSSExt(fn) {
		t.Errorf(`Expected file name "%s" to not have the ext`, fn)
	} //if

	// Check if a good file name has the extension
	if fn := "good.ext"; !hasSSExt(fn) {
		t.Errorf(`Expected file name "%s" to have the ext`, fn)
	} //if

	// Check if a good file name has the extension
	if fn := "good.txt.ext"; !hasSSExt(fn) {
		t.Errorf(`Expected file name "%s" to have the ext`, fn)
	} //if
} //func

func TestPrepAESKey(t *testing.T) {
	// Create a key that is less than 16 bytes
	txt := "12334"

	// Prepare the key
	key, err := prepAESKey(txt)

	// Check the error
	if err != nil {
		t.Fatal(err)
	} //if

	// Make sure the key is 16 bytes long
	if expected, actual := 16, len(key); actual != expected {
		t.Errorf("Expected key to be len %d but was %d", expected, actual)
	} //if

	// Create a key that is greater than 16 bytes buut less than 24 bytes
	txt = "12345678901234567"

	// Prepare the key
	key, err = prepAESKey(txt)

	// Check the error
	if err != nil {
		t.Fatal(err)
	} //if

	// Make sure the key is 16 bytes long
	if expected, actual := 24, len(key); actual != expected {
		t.Errorf("Expected key to be len %d but was %d", expected, actual)
	} //if

	// Create a key that is greater than 24 bytes buut less than 32 bytes
	txt = "1234567890123456789012345"

	// Prepare the key
	key, err = prepAESKey(txt)

	// Check the error
	if err != nil {
		t.Fatal(err)
	} //if

	// Make sure the key is 16 bytes long
	if expected, actual := 32, len(key); actual != expected {
		t.Errorf("Expected key to be len %d but was %d", expected, actual)
	} //if

	// Create a key that is greater than 32 bytes
	txt = "123456789012345678901234567890123"

	if _, err := prepAESKey(txt); err == nil {
		t.Error("Expected error from large key")
	} //if
} //func

func TestPad(t *testing.T) {
	// The buffer to pad
	var buff []byte

	// Pad a buffer to the correct size
	buff = pad(buff, 3)

	// Check the size of the buffer
	if expected, actual := 3, len(buff); actual != expected {
		t.Errorf("Expected buffer size to be %d but was %d", expected, actual)
	} //if

	// Pad a buffer to the correct size
	buff = pad(buff, 10)

	// Check the size of the buffer
	if expected, actual := 13, len(buff); actual != expected {
		t.Errorf("Expected buffer size to be %d but was %d", expected, actual)
	} //if
} //func
