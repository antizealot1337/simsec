package simsec

import (
	"crypto/aes"
	"crypto/cipher"
	"errors"
	"io"
	"os"
	"path"
)

const (
	ext = ".enc"
)

var (
	// ErrMissingExt is returned when attempting to decrypt a file that may not be
	// encrypted.
	ErrMissingExt = errors.New("missing extension")
)

// DecryptFile a file using the given key. File name is expected to have a ".ss"
// extension and the new file will be the same name without the extension.
func DecryptFile(file, key string) error {
	// Check the file extension
	if !hasSSExt(file) {
		return ErrMissingExt
	} //if

	// Open the source file
	src, err := os.Open(file)

	// Check for an error
	if err != nil {
		return err
	} //if

	// Make sure the file is closed
	defer src.Close()

	// Open the destination file
	dest, err := os.Create(file[:len(file)-3])

	// Check for an error
	if err != nil {
		return err
	} //if

	// Make sure the file is closed
	defer dest.Close()

	// Prepare the cipher key
	ckey, err := prepAESKey(key)

	// Check for an error
	if err != nil {
		return err
	} //if

	// Create the cipher
	block, err := aes.NewCipher(ckey)

	// Check for an error
	if err != nil {
		return err
	} //if

	return Decrypt(src, dest, block)
} //func

// EncryptFile the file using the given key. The encrypted output is written to
// a new file with the ".ss" extension.
func EncryptFile(file, key string) error {
	// Open the source file
	src, err := os.Open(file)

	// Check for an error
	if err != nil {
		return err
	} //if

	// Make sure the source file is closed
	defer src.Close()

	// Open the destination file
	dest, err := os.Create(file + ext)

	// Check for an error
	if err != nil {
		return err
	} //if

	// Make sure the destination file is closed
	defer dest.Close()

	// Prepare the cipher key
	ckey, err := prepAESKey(key)

	// Check for an error
	if err != nil {
		return err
	} //if

	// Create the cypher
	block, err := aes.NewCipher(ckey)

	// Check for an error
	if err != nil {
		return err
	} //if

	return Encrypt(src, dest, block)
} //func

// Decrypt data from the source to the destination using the provided block.
// Deprecated.
func Decrypt(src io.Reader, dest io.Writer, block cipher.Block) error {
	return Encrypt(src, dest, block)
} //func

// Encrypt data from the source to the desetinaion using the provided block.
func Encrypt(src io.Reader, dest io.Writer, block cipher.Block) error {
	// Create a zero IV
	iv := make([]byte, block.BlockSize())

	// Create the stream
	stream := cipher.NewOFB(block, iv)

	// Create a stream reader
	streamreader := &cipher.StreamReader{
		S: stream,
		R: src,
	}

	_, err := io.Copy(dest, streamreader)

	return err
} //func

func hasSSExt(file string) bool {
	return path.Ext(file) == ".ext"
} //func

func prepAESKey(txt string) ([]byte, error) {
	// Check if the key is of an invalid size
	if len(txt) > 32 {
		return nil, errors.New("Key is too large for AES cipher")
	} //if

	// Turn the txt into a byte slice
	key := []byte(txt)

	// Check if the key is okay
	if ks := len(key); ks == 16 || ks == 24 || ks == 32 {
		return key, nil
	} else if ks > 24 {
		// Pad the key to 32
		key = pad(key, 32-ks)
	} else if ks > 16 {
		// Pad the key to 24
		key = pad(key, 24-ks)
	} else {
		// Pad the key to 16
		key = pad(key, 16-ks)
	} //if

	// Adjust the key size
	return key, nil
} //func

func pad(buff []byte, amount int) []byte {
	// Loop to pad the buffer
	for i := 0; i < amount; i++ {
		buff = append(buff, ' ')
	} //for

	return buff
} //func
