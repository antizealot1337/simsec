# Simple Secruity

A command line tool that uses AES encryption to encrypt or decrypt files
written in Go. It will accept a key from the command line or if none is
provided it will prompt for one. Keys are provided as strings and will be
padded to make sure their size fits one of 16, 24, or 32 bytes. Keys that are
too long cause the program to terminate.

## Usage
For the most definative information see `enc -help`. A list of files may be
passed to the command and they will all be encrypted/decrypted using the same
key. To decrypt a file pass the `-decrypt` flag or if the command is called
under the name `dec` it will assume it should decrypt the files passed to it.
Decrypt expects files with the extension `.ext` which will be added
automatically when ecrypting files.

## License
Licensed under the terms of the MIT license. See license file for more
information.
