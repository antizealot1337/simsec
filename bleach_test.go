package simsec

import (
	"bytes"
	"errors"
	"testing"
)

func TestWriteZero(t *testing.T) {
	// Create a buffer
	var buff bytes.Buffer

	// Write zeros to the buffer
	if err := writeZeros(&buff, 3, 3); err != nil {
		t.Fatal("Unexpected error:", err)
	} //if

	// Check the size of the buffer
	if expected, actual := 3, buff.Len(); actual != expected {
		t.Errorf("Expected buffer size to be %d but was %d", expected, actual)
	} //if

	// Check the contents of the buffer
	for idx, val := range buff.Bytes() {
		if val != 0 {
			t.Errorf("Expected value at index %d to be 0 but was %d", idx, val)
		} //if
	} //for

	// Reset the buffer
	buff.Reset()

	// Write zeros to the buffer
	if err := writeZeros(&buff, 7, 3); err != nil {
		t.Fatal("Unexpected error:", err)
	} //if

	// Check the size of the buffer
	if expected, actual := 7, buff.Len(); actual != expected {
		t.Errorf("Expected buffer size to be %d but was %d", expected, actual)
	} //if

	// Check the contents of the buffer
	for idx, val := range buff.Bytes() {
		if val != 0 {
			t.Errorf("Expected value at index %d to be 0 but was %d", idx, val)
		} //if
	} //for
} //func

func TestZeroErrCtx(t *testing.T) {
	// Create an error
	err := errors.New("test")

	if expected, actual := "Zeroing error due to test", zeroErrCtx(err).Error(); actual != expected {
		t.Errorf(`Expected error to be "%s" but was "%s"`, expected, actual)
	} //if
} //func
